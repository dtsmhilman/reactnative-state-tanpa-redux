import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function App() {

  const [angka, setAngka] = useState(0);
  function tambahSatu() {
    let nilai = angka + 1
    setAngka(nilai)
  }
  function kurangSatu() {
    let nilai = angka - 1
    setAngka(nilai)
  }



  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.tombol} onPress={tambahSatu}>
        <Text style={styles.textTombol} >Tambah Satu</Text>
      </TouchableOpacity>
  <Text style={styles.textAngka}>{angka}</Text>
      <TouchableOpacity style={styles.tombol} onPress={kurangSatu}>
        <Text style={styles.textTombol} >Kurang Satu</Text>
      </TouchableOpacity>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 50,
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  tombol: {
    backgroundColor: '#2196F3',
    padding: 30,
    alignItems: 'center',
    width: '30%',
    alignSelf: 'center',
  },
  textTombol: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  },
  textAngka: {
    textAlign: 'center',
    fontSize: 200,
    fontWeight: 'bold',
  }
});
